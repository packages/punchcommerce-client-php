<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace Unit;

use PHPUnit\Framework\TestCase;
use PunchCommerce\Structs\Product;

/**
 * Class ProductTest
 * @package Unit
 */
class ProductTest extends TestCase
{
    public function testProductSerialization()
    {
        $product = new Product();

        $product->ordernumber = '1234';
        $product->brand = 'Patrick Brand';
        $product->title = 'Patrick Product';
        $product->description = 'Lorem Ipsum Star Wars';
        $product->price = 42.00;
        $product->taxRate = 16;
        $product->packagingUnit = 'STÜCK';
        $product->weight = 25.52;
        $product->currency = 'USD';
        $product->shippingTime = 3;
        $product->active = true;

        self::assertJson(json_encode($product));

        $encodedProduct = json_decode(json_encode($product));

        self::assertSame($product->title, $encodedProduct->title);
    }
}