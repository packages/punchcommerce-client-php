<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace Unit;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PunchCommerce\ProductContext;
use PunchCommerce\ProductClient;
use PunchCommerce\Response\ReadProductResponse;
use PunchCommerce\Structs\Product;
use Ramsey\Uuid\Uuid;

/**
 * Class ProductClientTest
 * @package Unit
 */
class ProductClientTest extends TestCase
{
    const API_KEY = '30caf28a-bd46-11eb-9b80-0242ac120005';
    const API_URL = 'http://punchcommerce.local/api/v1/';
    
    public function testCreateProductClient()
    {
        $guzzle = new Client();
        $context = ProductContext::fromToken(Uuid::uuid1());
        $client = new ProductClient($guzzle, $context);

        self::assertInstanceOf(ProductClient::class, $client);
    }

    public function testCreateProduct()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);

        $client = new ProductClient($guzzle, $context);

        $product = new Product();
        $product->ordernumber = '100000';
        $product->brand = 'Patrick Brand';
        $product->title = 'Patrick Product';
        $product->description = 'Lorem Ipsum Star Wars';
        $product->price = 42.00;
        $product->taxRate = 16;
        $product->packagingUnit = 'STÜCK';
        $product->weight = 25.52;
        $product->currency = 'USD';
        $product->shippingTime = 3;
        $product->active = true;

        $response = $client->createProduct($product);

        self::assertInstanceOf(Product::class, $response);
    }

    public function testListProducts()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);

        $client = new ProductClient($guzzle, $context);

        $products = $client->listProducts();

        self::assertInstanceOf(Product::class, array_pop($products));
        self::assertIsIterable($products);
    }

    public function testReadProduct()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);

        $client = new ProductClient($guzzle, $context);
        $response = $client->readProduct('100000');

        self::assertInstanceOf(Product::class, $response);
    }

    public function testUpdateProduct()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);

        $client = new ProductClient($guzzle, $context);

        $product = new Product();
        $product->ordernumber = '100000';
        $product->brand = 'Mighty Brand';
        $product->title = 'Our cool product';
        $product->description = 'Lorem Ipsum Star Wars';
        $product->price = 42.00;
        $product->taxRate = 16;
        $product->packagingUnit = 'PCE';
        $product->weight = 25.52;
        $product->currency = 'USD';
        $product->shippingTime = 3;
        $product->classificationType = 'ECMO';
        $product->classification = '1';
        $product->active = true;

        $response = $client->updateProduct($product);

        self::assertInstanceOf(Product::class, $response);
        self::assertEquals($product->brand, $response->brand);
    }

    public function testCanDeleteProduct()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);
        $client = new ProductClient($guzzle, $context);
        $response = $client->removeProduct("100000");

        self::assertTrue($response);
    }

    public function testCanDeleteAllProducts()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);
        $client = new ProductClient($guzzle, $context);
        $response = $client->removeProducts();

        self::assertTrue($response);
    }

    public function testCanValidateValidToken()
    {
        $guzzle = new Client();
        $context = new ProductContext(self::API_KEY, self::API_URL);
        $client = new ProductClient($guzzle, $context);
        $response = $client->validate();

        self::assertTrue($response);
    }

    public function testCanValidateInvalidToken()
    {
        $guzzle = new Client();
        $context = new ProductContext("d7a141c2-2e3a-11eb-9196-0242ac120005", self::API_URL);
        $client = new ProductClient($guzzle, $context);
        $response = $client->validate();

        self::assertFalse($response);
    }
}