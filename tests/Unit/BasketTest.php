<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace Unit;

use PHPUnit\Framework\TestCase;
use PunchCommerce\ProductContext;
use PunchCommerce\Structs\Basket;
use PunchCommerce\Structs\LineItem;
use PunchCommerce\Structs\Product;
use Ramsey\Uuid\Uuid;

/**
 * Class BasketTest
 * @package PunchCommerce\Tests
 */
class BasketTest extends TestCase
{
    public function testCanAddItemToBasket()
    {
        $product = new Product();
        $product->ordernumber = '100000';
        $product->brand = 'Patrick Brand';
        $product->title = 'Patrick Product';
        $product->description = 'Lorem Ipsum Star Wars';
        $product->price = 210;
        $product->taxRate = 16;
        $product->packagingUnit = 'STÜCK';
        $product->weight = 25.52;
        $product->currency = 'USD';
        $product->shippingTime = 3;
        $product->active = true;

        $item = new LineItem($product, 5);

        self::assertSame($item->productName, $product->title);
        self::assertSame($item->getQuantity(), 5);
        self::assertEquals(210, $item->priceNet);

        $basket = new Basket();
        $basket->add($item);
    }

    public function testCanSerializeBasket()
    {
        $product = new Product();
        $product->id = 21312432343;
        $product->ordernumber = '23762';
        $product->brandOrdernumber = 67906;
        $product->brand = 'Netzdirektion';
        $product->title = 'Pergamentpapier';
        $product->category = 'Testkategorie';
        $product->description = '1/8 Bogen weiß';
        $product->descriptionLong = '67906 Pergamentpapier 1/8 Bogen weiß';
        $product->imageUrl = 'https://www.lebensmittel-becker.de/oci/67906.jpg';
        $product->price = 2.2042016806723;
        $product->taxRate = 19;
        $product->purchaseUnit = 12.5;
        $product->referenceUnit = 1;
        $product->unit = 'kg';
        $product->unitName = 'kg';
        $product->packagingUnit = 'Karton';
        $product->weight = 0;
        $product->currency = 'USD';
        $product->shippingTime = 8;
        $product->classificationType = 'ECLASS';
        $product->classification = "90909090";
        $product->customField1 = '0.19';
        $product->customField2 = "BX";

        $item = new LineItem($product, 1);

        $basket = new Basket();
        $basket->add($item);

        self::assertJson(json_encode($item));
        self::assertJsonStringEqualsJsonFile('tests/Fixtures/ValidBasket.json', json_encode($basket));
    }
}