<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace Unit;

use PHPUnit\Framework\TestCase;
use PunchCommerce\ProductContext;
use Ramsey\Uuid\Uuid;

/**
 * Class ContextTest
 * @package PunchCommerceApi\tests\Unit
 */
class ContextTest extends TestCase
{
    public function testCanCreateContextObject()
    {
        $context = new ProductContext(Uuid::uuid1());
        self::assertInstanceOf(ProductContext::class, $context);
    }

    public function testGetToken()
    {
        $uuid = Uuid::uuid1();
        $context = new ProductContext($uuid);
        $token = $context->getToken();
        self::assertTrue(Uuid::isValid($token));
    }
}