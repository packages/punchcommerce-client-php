<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Requests;


use PunchCommerce\ProductContext;
use PunchCommerce\Structs\HttpMethod;

class ListProductsRequest extends Request
{
    const API_ENDPOINT = 'products';

    /**
     * @param ProductContext $context
     * @return static
     */
    public static function fromContext(ProductContext $context): self
    {
        return new self(HttpMethod::GET, $context->getUrl() . self::API_ENDPOINT, [
            'Content-Type' => 'application/json'
        ]);
    }
}