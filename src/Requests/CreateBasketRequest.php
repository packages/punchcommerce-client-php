<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Requests;

use PunchCommerce\GatewayContext;
use PunchCommerce\Structs\Basket;
use PunchCommerce\Structs\HttpMethod;

/**
 * Class CreateBasketRequest
 * @package PunchCommerce\Requests
 */
class CreateBasketRequest extends Request
{
    const API_ENDPOINT = 'basket';

    public static function fromBasket(Basket $basket, GatewayContext $context): self
    {
        $url = $context->getUrl()->withQuery('sID=' .  $context->getSID() .  '&uID=' . $context->getUID());
        return new self(HttpMethod::POST, $url . self::API_ENDPOINT, [
            'Content-Type' => 'application/json'
        ], json_encode($basket));
    }
}