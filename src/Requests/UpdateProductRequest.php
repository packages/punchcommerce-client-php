<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Requests;


use PunchCommerce\ProductContext;
use PunchCommerce\Structs\HttpMethod;
use PunchCommerce\Structs\Product;

/**
 * Class UpdateProductRequest
 * @package PunchCommerce\Requests
 */
class UpdateProductRequest extends Request
{
    const API_ENDPOINT = 'products/';

    /**
     * @param Product $product
     * @param ProductContext $context
     *
     * @return static
     */
    public static function fromProduct(Product $product, ProductContext $context): self
    {
        return new self(HttpMethod::PUT, $context->getUrl() . self::API_ENDPOINT . $product->ordernumber, [
            'Content-Type' => 'application/json'
        ], json_encode($product));
    }
}