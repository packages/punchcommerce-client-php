<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Requests;

use PunchCommerce\ProductContext;
use PunchCommerce\Structs\HttpMethod;
use PunchCommerce\Structs\Product;

/**
 * Class CreateProductRequest
 * @package PunchCommerce\Requests
 */
class CreateProductRequest extends Request
{
    const API_ENDPOINT = 'products';

    /**
     * @param Product $product
     * @param ProductContext $context
     *
     * @return static
     */
    public static function fromProduct(Product $product, ProductContext $context): self
    {
        return new self(HttpMethod::POST, $context->getUrl() . self::API_ENDPOINT, [
            'Content-Type' => 'application/json'
        ], json_encode($product));
    }
}