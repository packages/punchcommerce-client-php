<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Requests;

use PunchCommerce\ProductContext;
use PunchCommerce\Structs\HttpMethod;

/**
 * Class ValidateTokenRequest
 * @package PunchCommerce\Requests
 */
class ValidateTokenRequest extends Request
{
    const API_ENDPOINT = 'validate';

    /**
     * @param ProductContext $context
     * @return static
     */
    public static function fromContext(ProductContext $context): self
    {
        return new self(HttpMethod::GET, $context->getUrl() . self::API_ENDPOINT, [
            'Content-Type' => 'application/json'
        ]);
    }
}