<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Exceptions;

use Throwable;

/**
 * Class InvalidApiResultException
 * @package PunchCommerce\Exceptions
 */
class InvalidApiResultException extends \Exception
{
    public function __construct(int $status)
    {
        parent::__construct("Api-Server returned HTTP status code: {$status}.", $status);
    }
}