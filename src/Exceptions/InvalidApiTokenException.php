<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Exceptions;

/**
 * Class InvalidApiTokenException
 * @package PunchCommerceApi\Exceptions
 */
class InvalidApiTokenException extends \Exception
{

}