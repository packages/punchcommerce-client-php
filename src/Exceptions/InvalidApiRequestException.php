<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Exceptions;

/**
 * Class InvalidApiRequestException
 * @package PunchCommerce\Exceptions
 */
class InvalidApiRequestException extends \Exception
{

    /**
     * InvalidApiRequestException constructor.
     * @param int $status
     * @param string|null $message
     */
    public function __construct(int $status, ?string $message)
    {
        $exception = "API-Server returned HTTP status code {$status}";

        if ($message) {
            $exception .= " with message: {$message}";
        }

        parent::__construct($exception, $status);
    }
}