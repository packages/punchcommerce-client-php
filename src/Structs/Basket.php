<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Structs;

/**
 * Class Basket
 * @package PunchCommerce\Structs
 */
class Basket implements \JsonSerializable
{
    /**
     * @var array|LineItem[]
     */
    private array $basket = [];

    /**
     * Add new LineItem to basket
     *
     * @param LineItem $lineItem
     * @return bool
     */
    public function add(LineItem $lineItem): bool
    {
        foreach ($this->basket as $item) {
            if ($item->productOrdernumber == $lineItem->productOrdernumber) {
                return false;
            }
        }
        $this->basket[] = $lineItem;
        return true;
    }

    /**
     * Remove single item from basket by ordernumber
     *
     * @param string $ordernumber
     * @return bool
     */
    public function remove(string $ordernumber): bool
    {
        foreach ($this->basket as $index => $item) {
            if ($item->productOrdernumber === $ordernumber) {
                unset($this->basket[$index]);
                return true;
            }
        }
        return false;
    }

    /**
     * @return iterable|LineItem[]
     */
    public function getBasket(): iterable
    {
        return $this->basket;
    }

    /**
     * @return array|mixed|LineItem[]
     */
    public function jsonSerialize()
    {
        return [
            'basket' => $this->basket
        ];
    }

    /**
     * Removes all items from basket
     */
    public function clear(): void
    {
        $this->basket = [];
    }
}