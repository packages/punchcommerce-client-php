<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Structs;


interface ProductInterface
{
    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return string
     * @deprecated
     */
    public function toApiRequest(): string;
}