<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Structs;

/**
 * Class LineItem
 * @package PunchCommerce\Structs
 */
class LineItem implements \JsonSerializable
{
    public const PRODUCT_TYPE = 'product';
    public const SHIPPING_COSTS_TYPE = 'shipping-costs';

    /**
     * @var string
     */
    public string $productOrdernumber;

    /**
     * @var string
     */
    public string $productName;

    /**
     * @var int
     */
    public int $quantity;

    /**
     * @var float
     */
    public float $itemPrice;

    /**
     * @var float
     */
    public float $price;

    /**
     * @var float
     */
    public float $priceNet;

    /**
     * @var float
     */
    public float $taxRate;

    /**
     * @var string
     */
    public string $type;

    /**
     * @var Product
     */
    public Product $product;

    /**
     * LineItem constructor.
     * @param Product $product
     * @param int $quantity
     * @param string $type
     */
    public function __construct(Product $product, int $quantity, string $type = self::PRODUCT_TYPE)
    {
        $this->product = $product;
        $this->productName = $this->product->getTitle();
        $this->productOrdernumber = $this->product->getOrdernumber();
        $this->taxRate = $this->product->getTaxRate();
        $this->priceNet = $this->product->getPrice() * $quantity;
        $this->price = round($this->priceNet * (($this->taxRate/100) + 1), 4);
        $this->itemPrice = $this->product->getPrice();
        $this->quantity = $quantity;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getProductOrdernumber(): string
    {
        return $this->productOrdernumber;
    }

    /**
     * @param string $productOrdernumber
     * @return LineItem
     */
    public function setProductOrdernumber(string $productOrdernumber): LineItem
    {
        $this->productOrdernumber = $productOrdernumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     * @return LineItem
     */
    public function setProductName(string $productName): LineItem
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return LineItem
     */
    public function setQuantity(int $quantity): LineItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getItemPrice(): float
    {
        return $this->itemPrice;
    }

    /**
     * @param float $itemPrice
     * @return LineItem
     */
    public function setItemPrice(float $itemPrice): LineItem
    {
        $this->itemPrice = $itemPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return LineItem
     */
    public function setPrice(float $price): LineItem
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceNet(): float
    {
        return $this->priceNet;
    }

    /**
     * @param float $priceNet
     * @return LineItem
     */
    public function setPriceNet(float $priceNet): LineItem
    {
        $this->priceNet = $priceNet;
        return $this;
    }

    /**
     * @return float
     */
    public function getTaxRate(): float
    {
        return $this->taxRate;
    }

    /**
     * @param float $taxRate
     * @return LineItem
     */
    public function setTaxRate(float $taxRate): LineItem
    {
        $this->taxRate = $taxRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return LineItem
     */
    public function setType(string $type): LineItem
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return LineItem
     */
    public function setProduct(Product $product): LineItem
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return \stdClass
     */
    public function jsonSerialize(): \stdClass
    {
        $properties = get_object_vars($this);
        $data = new \stdClass();

        if (is_array($properties)) {
            foreach ($properties as $property => $value) {
                $name = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $property)), '_');
                $data->{$name} = $value;
            }
        }

        return $data;
    }
}