<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Structs;

/**
 * Class HttpMethod
 * @package PunchCommerce\Structs
 */
class HttpMethod
{
    public const POST = 'POST';
    public const GET = 'GET';
    public const DELETE = 'DELETE';
    public const PUT = 'PUT';
}