<?php declare(strict_types=1);
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Structs;

use PunchCommerce\Response\PunchCommerceApiResponse;
use PunchCommerce\Traits\MinimumProductDataTrait;

/**
 * Class Product
 * @package PunchCommerceApi\Structs
 */
class Product implements ProductInterface, \JsonSerializable
{
    use MinimumProductDataTrait;

    public ?string $id = null;
    public ?string $brandOrdernumber = null;

    public ?string $customerOrdernumber = null;
    public ?string $category = null;
    public ?string $descriptionLong = null;
    public ?string $imageUrl = null;
    public ?float $purchaseUnit = null;
    public ?float $referenceUnit = null;
    public ?string $graduatedPrices = null;
    public ?string $currency = null;
    public ?int $purchaseSteps = null;
    public ?int $minPurchase = null;
    public ?int $maxPurchase = null;
    public ?string $unit = null;
    public ?string $unitName = null;
    public ?string $customField1 = null;
    public ?string $customField2 = null;
    public ?string $customField3 = null;
    public ?string $customField4 = null;
    public ?string $customField5 = null;
    public ?string $customField6 = null;
    public ?string $customField7 = null;
    public ?string $customField8 = null;
    public ?string $customField9 = null;
    public ?string $customField10 = null;

    /**
     * @return string|null
     */
    public function getBrandOrdernumber(): ?string
    {
        return $this->brandOrdernumber;
    }

    /**
     * @param string|null $brandOrdernumber
     * @return Product
     */
    public function setBrandOrdernumber(?string $brandOrdernumber): Product
    {
        $this->brandOrdernumber = $brandOrdernumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param string|null $category
     * @return Product
     */
    public function setCategory(?string $category): Product
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescriptionLong(): ?string
    {
        return $this->descriptionLong;
    }

    /**
     * @param string|null $descriptionLong
     * @return Product
     */
    public function setDescriptionLong(?string $descriptionLong): Product
    {
        $this->descriptionLong = $descriptionLong;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string|null $imageUrl
     * @return Product
     */
    public function setImageUrl(?string $imageUrl): Product
    {
        $this->imageUrl = $imageUrl;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPurchaseUnit(): ?float
    {
        return $this->purchaseUnit;
    }

    /**
     * @param float|null $purchaseUnit
     * @return Product
     */
    public function setPurchaseUnit(?float $purchaseUnit): Product
    {
        $this->purchaseUnit = $purchaseUnit;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getReferenceUnit(): ?float
    {
        return $this->referenceUnit;
    }

    /**
     * @param float|null $referenceUnit
     * @return Product
     */
    public function setReferenceUnit(?float $referenceUnit): Product
    {
        $this->referenceUnit = $referenceUnit;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGraduatedPrices(): ?string
    {
        return $this->graduatedPrices;
    }

    /**
     * @param string|null $graduatedPrices
     * @return Product
     */
    public function setGraduatedPrices(?string $graduatedPrices): Product
    {
        $this->graduatedPrices = $graduatedPrices;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     * @return Product
     */
    public function setCurrency(?string $currency): Product
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPurchaseSteps(): ?int
    {
        return $this->purchaseSteps;
    }

    /**
     * @param int|null $purchaseSteps
     * @return Product
     */
    public function setPurchaseSteps(?int $purchaseSteps): Product
    {
        $this->purchaseSteps = $purchaseSteps;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinPurchase(): ?int
    {
        return $this->minPurchase;
    }

    /**
     * @param int|null $minPurchase
     * @return Product
     */
    public function setMinPurchase(?int $minPurchase): Product
    {
        $this->minPurchase = $minPurchase;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxPurchase(): ?int
    {
        return $this->maxPurchase;
    }

    /**
     * @param int|null $maxPurchase
     * @return Product
     */
    public function setMaxPurchase(?int $maxPurchase): Product
    {
        $this->maxPurchase = $maxPurchase;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $unit
     * @return Product
     */
    public function setUnit(?string $unit): Product
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnitName(): ?string
    {
        return $this->unitName;
    }

    /**
     * @param string|null $unitName
     * @return Product
     */
    public function setUnitName(?string $unitName): Product
    {
        $this->unitName = $unitName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField1(): ?string
    {
        return $this->customField1;
    }

    /**
     * @param string|null $customField1
     * @return Product
     */
    public function setCustomField1(?string $customField1): Product
    {
        $this->customField1 = $customField1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField2(): ?string
    {
        return $this->customField2;
    }

    /**
     * @param string|null $customField2
     * @return Product
     */
    public function setCustomField2(?string $customField2): Product
    {
        $this->customField2 = $customField2;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField3(): ?string
    {
        return $this->customField3;
    }

    /**
     * @param string|null $customField3
     * @return Product
     */
    public function setCustomField3(?string $customField3): Product
    {
        $this->customField3 = $customField3;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField4(): ?string
    {
        return $this->customField4;
    }

    /**
     * @param string|null $customField4
     * @return Product
     */
    public function setCustomField4(?string $customField4): Product
    {
        $this->customField4 = $customField4;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField5(): ?string
    {
        return $this->customField5;
    }

    /**
     * @param string|null $customField5
     * @return Product
     */
    public function setCustomField5(?string $customField5): Product
    {
        $this->customField5 = $customField5;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField6(): ?string
    {
        return $this->customField6;
    }

    /**
     * @param string|null $customField6
     * @return Product
     */
    public function setCustomField6(?string $customField6): Product
    {
        $this->customField6 = $customField6;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField7(): ?string
    {
        return $this->customField7;
    }

    /**
     * @param string|null $customField7
     * @return Product
     */
    public function setCustomField7(?string $customField7): Product
    {
        $this->customField7 = $customField7;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField8(): ?string
    {
        return $this->customField8;
    }

    /**
     * @param string|null $customField8
     * @return Product
     */
    public function setCustomField8(?string $customField8): Product
    {
        $this->customField8 = $customField8;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField9(): ?string
    {
        return $this->customField9;
    }

    /**
     * @param string|null $customField9
     * @return Product
     */
    public function setCustomField9(?string $customField9): Product
    {
        $this->customField9 = $customField9;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomField10(): ?string
    {
        return $this->customField10;
    }

    /**
     * @param string|null $customField10
     * @return Product
     */
    public function setCustomField10(?string $customField10): Product
    {
        $this->customField10 = $customField10;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return true;
    }

    /**
     * @param \stdClass $response
     * @return static
     */
    public static function fromApiResponse(\stdClass $data): self
    {
        $self = new self();

        $properties = get_object_vars($data);

        if (is_array($properties)) {
            foreach ($properties as $property => $value) {
                $separator = '_';
                $property = str_replace($separator, '', ucwords($property, $separator));;
                $method = "set" . ucfirst($property);

                if (method_exists($self, $method)) {
                    $self->{$method}($value);
                }
            }
        }

        return $self;
    }

    /**
     * @return \stdClass
     */
    public function jsonSerialize(): \stdClass
    {
        $properties = get_object_vars($this);
        $data = new \stdClass();

        if (is_array($properties)) {
            foreach ($properties as $property => $value) {
                $name = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $property)), '_');
                $data->{$name} = $value;
            }
        }

        return $data;
    }

    /**
     * @return string
     * @deprecated Use JsonSerializable instead
     */
    public function toApiRequest(): string
    {
        $properties = get_object_vars($this);
        $data = new \stdClass();

        if (is_array($properties)) {
            foreach ($properties as $property => $value) {
                $name = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $property)), '_');
                $data->{$name} = $value;
            }
        }

        return json_encode($data);
    }

    public function getCustomerOrdernumber(): ?string
    {
        return $this->customerOrdernumber;
    }

    public function setCustomerOrdernumber(?string $customerOrdernumber): Product
    {
        $this->customerOrdernumber = $customerOrdernumber;
        return $this;
    }
}
