<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce;

use PunchCommerce\Exceptions\InvalidApiTokenException;
use PunchCommerce\Requests\Uri;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class PunchoutContext
 * @package PunchCommerce
 */
class GatewayContext
{
    public const API_VERSION = 'v1';
    public const URL = 'https://www.punchcommerce.de/gateway/';

    private Uri $uri;
    private UuidInterface $token;
    private string $sID;
    private string $uID;

    /**
     * PunchoutContext constructor.
     *
     * @param string $token
     * @param string $sID
     * @param string $uID
     * @param string|null $url
     * @throws InvalidApiTokenException
     */
    public function __construct(
        string $token,
        string $sID,
        string $uID,
        ?string $url = null)
    {
        if (!Uuid::isValid($token)) {
            throw new InvalidApiTokenException('Token is malformed');
        }

        $this->sID = $sID;
        $this->uID = $uID;
        $this->uri = new Uri($url ?? self::URL . self::API_VERSION . '/oci/punchout');
        $this->token = Uuid::fromString($token);
    }

    /**
     * @return string
     */
    public function getSID(): string
    {
        return $this->sID;
    }

    /**
     * @return string
     */
    public function getUID(): string
    {
        return $this->uID;
    }

    /**
     * @param string $token
     * @return $this
     *
     * @throws InvalidApiTokenException
     */
    public static function fromToken(string $token): self
    {
        return new self($token);
    }

    /**
     * @return UuidInterface
     */
    public function getToken(): UuidInterface
    {
        return $this->token;
    }

    /**
     * @return Uri
     */
    public function getUrl(): Uri
    {
        return $this->uri;
    }
}