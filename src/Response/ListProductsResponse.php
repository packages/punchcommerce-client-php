<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Response;

use Psr\Http\Message\ResponseInterface;
use PunchCommerce\Structs\Product;

/**
 * Class ListProductsResponse
 * @package PunchCommerce\Response
 */
class ListProductsResponse extends PunchCommerceApiResponse
{
    public iterable $products;

    /**
     * @param ResponseInterface $response
     * @return static
     */
    public static function fromHttpResponse(ResponseInterface $response): self
    {
        $apiResponse = new self($response);

        if ($apiResponse->success) {
            if (is_iterable($apiResponse->data->data)) {
                foreach ($apiResponse->data->data as $product) {
                    $apiResponse->products[] = Product::fromApiResponse($product);
                }
            }
        }

        return $apiResponse;
    }
}