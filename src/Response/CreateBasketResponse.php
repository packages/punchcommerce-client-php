<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Response;

use Psr\Http\Message\ResponseInterface;

class CreateBasketResponse extends PunchCommerceApiResponse
{
    public ?array $basket;

    public static function fromHttpResponse(ResponseInterface $response): self
    {
        $apiResponse = new self($response);

        if ($apiResponse->success) {
            $apiResponse->basket = $apiResponse->data['basketItems'];
        }

        return $apiResponse;
    }
}