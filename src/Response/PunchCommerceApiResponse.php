<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Response;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ApiResponse
 * @package PunchCommerce\Response
 */
abstract class PunchCommerceApiResponse
{
    /**
     * @var \stdClass|mixed|null
     */
    public \stdClass $data;

    /**
     * @var bool
     */
    public bool $success = false;

    /**
     * @var string
     */
    public ?string $message = null;

    /**
     * @var int
     */
    public int $status = 200;

    /**
     * @var ResponseInterface
     */
    private ResponseInterface $response;

    /**
     * ApiResponse constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
        $this->status = $this->response->getStatusCode();
        $this->data = json_decode($this->response->getBody()->getContents());

        if (property_exists($this->data, 'success')) {
            $this->success = $this->data->success;
        }

        if (property_exists($this->data, 'message')) {
            $this->message = $this->data->message;
        }
    }
}