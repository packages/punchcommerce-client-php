<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Response;


use Psr\Http\Message\ResponseInterface;
use PunchCommerce\Structs\Product;

/**
 * Class UpdateProductResponse
 * @package PunchCommerce\Response
 */
class UpdateProductResponse extends PunchCommerceApiResponse
{
    public ?Product $product;

    /**
     * @param ResponseInterface $response
     * @return static
     */
    public static function fromHttpResponse(ResponseInterface $response): self
    {
        $apiResponse = new self($response);

        if ($apiResponse->success) {
            $apiResponse->product = Product::fromApiResponse($apiResponse->data->data);
        }

        return $apiResponse;
    }
}