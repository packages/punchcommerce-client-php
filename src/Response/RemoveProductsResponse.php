<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Response;

use Psr\Http\Message\ResponseInterface;
use PunchCommerce\Structs\Product;

/**
 * Class RemoveProductsResponse
 * @package PunchCommerce\Response
 */
class RemoveProductsResponse extends PunchCommerceApiResponse
{
    /**
     * @param ResponseInterface $response
     * @return static
     */
    public static function fromHttpResponse(ResponseInterface $response): self
    {
        return new self($response);
    }
}