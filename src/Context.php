<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce;

use PunchCommerce\Exceptions\InvalidApiTokenException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Context
 * @package PunchCommerceApi
 * @deprecated Use ProductContext instead, this will be removed in future versions
 */
class Context
{
    public const API_VERSION = 'v1';
    public const URL = 'https://www.punchcommerce.de/api/';
    private UuidInterface $token;
    private ?string $url;

    /**
     * Context constructor.
     *
     * @param string $token
     * @param string $url
     *
     * @throws InvalidApiTokenException
     */
    public function __construct(string $token, string $url = null)
    {
        if (!Uuid::isValid($token)) {
            throw new InvalidApiTokenException('Customer token is invalid');
        }

        $this->token = Uuid::fromString($token);
        $this->url = ($url ?? self::URL . self::API_VERSION . '/');
    }

    /**
     * @param string $token
     * @return $this
     *
     * @throws InvalidApiTokenException
     */
    public static function fromToken(string $token): self
    {
        return new self($token);
    }

    /**
     * @return UuidInterface
     */
    public function getToken(): UuidInterface
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}