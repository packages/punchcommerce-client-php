<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use PunchCommerce\Exceptions\InvalidApiRequestException;
use PunchCommerce\Exceptions\InvalidApiResultException;
use PunchCommerce\Requests\CreateProductRequest;
use PunchCommerce\Requests\ListProductsRequest;
use PunchCommerce\Requests\ReadProductRequest;
use PunchCommerce\Requests\RemoveProductRequest;
use PunchCommerce\Requests\RemoveProductsRequest;
use PunchCommerce\Requests\UpdateProductRequest;
use PunchCommerce\Requests\ValidateTokenRequest;
use PunchCommerce\Response\CreateProductResponse;
use PunchCommerce\Response\ListProductsResponse;
use PunchCommerce\Response\ReadProductResponse;
use PunchCommerce\Response\RemoveProductResponse;
use PunchCommerce\Response\RemoveProductsResponse;
use PunchCommerce\Response\UpdateProductResponse;
use PunchCommerce\Structs\Product;

/**
 * Class Handler
 * @package PunchCommerceApi
 */
class ProductClient
{
    /**
     * @var ProductContext
     */
    protected ProductContext $context;

    /**
     * @var ClientInterface
     */
    protected ClientInterface $client;

    /**
     * ProductClient constructor.
     *
     * @param ClientInterface $client
     * @param ProductContext $context
     */
    public function __construct(ClientInterface $client, ProductContext $context)
    {
        $this->client = $client;
        $this->context = $context;
    }

    /**
     * @param Product $product
     * @return Product|null
     *
     * @throws InvalidApiRequestException
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function createProduct(Product $product): ?Product
    {
        $response = $this->request(CreateProductRequest::fromProduct($product, $this->context));

        if ($response->getStatusCode() >= 500) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = CreateProductResponse::fromHttpResponse($response);

        if ($response->status >= 400) {
            throw new InvalidApiRequestException($response->status, $response->message);
        }

        if (! $response->success) {
            return null;
        }

        return $response->product;
    }

    /**
     * @param string $ordernumber
     * @return Product|null
     *
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function readProduct(string $ordernumber): ?Product
    {
        $response = $this->request(ReadProductRequest::fromOrdernumber($ordernumber, $this->context));

        if ($response->getStatusCode() > 200) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = ReadProductResponse::fromHttpResponse($response);

        if (! $response->success) {
            return null;
        }

        return $response->product;
    }

    /**
     * @param Product $product
     * @return Product|null
     *
     * @throws InvalidApiRequestException
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function updateProduct(Product $product): ?Product
    {
        $response = $this->request(UpdateProductRequest::fromProduct($product, $this->context));

        if ($response->getStatusCode() >= 500) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = UpdateProductResponse::fromHttpResponse($response);

        if ($response->status >= 400) {
            throw new InvalidApiRequestException($response->status, $response->message);
        }

        if (! $response->success) {
            return null;
        }

        return $response->product;
    }

    /**
     * @param string $ordernumber
     * @return bool
     *
     * @throws InvalidApiRequestException
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function removeProduct(string $ordernumber): bool
    {
        $response = $this->request(RemoveProductRequest::fromOrdernumber($ordernumber, $this->context));

        if ($response->getStatusCode() >= 500) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = RemoveProductResponse::fromHttpResponse($response);

        if ($response->status >= 400) {
            throw new InvalidApiRequestException($response->status, $response->message);
        }

        return $response->success;
    }

    /**
     * @return bool
     *
     * @throws InvalidApiRequestException
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function removeProducts(): bool
    {
        $response = $this->request(RemoveProductsRequest::fromContext($this->context));

        if ($response->getStatusCode() >= 500) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = RemoveProductsResponse::fromHttpResponse($response);

        if ($response->status >= 400) {
            throw new InvalidApiRequestException($response->status, $response->message);
        }

        return $response->success;
    }

    /**
     * @return iterable|null
     *
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function listProducts(): ?iterable
    {
        $response = $this->request(ListProductsRequest::fromContext($this->context));

        if ($response->getStatusCode() > 200) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = ListProductsResponse::fromHttpResponse($response);

        if (! $response->success) {
            return null;
        }

        return $response->products;
    }


    /**
     * @return bool
     *
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function validate(): bool {
        $response = $this->request(ValidateTokenRequest::fromContext($this->context));

        if ($response->getStatusCode() > 200) {
            return false;
        }

        return true;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     *
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function request(RequestInterface $request): ResponseInterface
    {
        $request = $request->withAddedHeader('Authorization', 'Bearer ' . $this->context->getToken());
        return $this->client->sendRequest($request);
    }
}