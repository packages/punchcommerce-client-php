<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce\Traits;

/**
 * Trait MinimumProductData
 * @package PunchCommerce\Traits
 */
trait MinimumProductDataTrait
{
    public string $ordernumber;
    public string $brand;
    public string $title;
    public string $description;
    public float $price;
    public float $taxRate;
    public string $packagingUnit;
    public float $weight;
    public int $shippingTime;
    public string $classificationType;
    public string $classification;
    public bool $active;

    /**
     * @return string
     */
    public function getOrdernumber(): string
    {
        return $this->ordernumber;
    }

    /**
     * @param string $ordernumber
     */
    public function setOrdernumber(string $ordernumber): void
    {
        $this->ordernumber = $ordernumber;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getTaxRate(): float
    {
        return $this->taxRate;
    }

    /**
     * @param float $taxRate
     */
    public function setTaxRate(float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return string
     */
    public function getPackagingUnit(): string
    {
        return $this->packagingUnit;
    }

    /**
     * @param string $packagingUnit
     */
    public function setPackagingUnit(string $packagingUnit): void
    {
        $this->packagingUnit = $packagingUnit;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getShippingTime(): int
    {
        return $this->shippingTime;
    }

    /**
     * @param int $shippingTime
     */
    public function setShippingTime(int $shippingTime): void
    {
        $this->shippingTime = $shippingTime;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive($active): void
    {
        $this->active = (bool) $active;
    }
}