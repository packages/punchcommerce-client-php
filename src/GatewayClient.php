<?php
/*
 *  punchcommerce.de
 *
 *  @copyright: Copyright (c) netzdirektion | Gesellschaft für digitale Wertarbeit mbH, 2021
 *  @link: https://netzdirektion.de
 *  @link: https://punchcommerce.de
 */

namespace PunchCommerce;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use PunchCommerce\Exceptions\InvalidApiRequestException;
use PunchCommerce\Exceptions\InvalidApiResultException;
use PunchCommerce\Requests\CreateBasketRequest;
use PunchCommerce\Response\CreateBasketResponse;
use PunchCommerce\Structs\Basket;

class GatewayClient
{
    protected GatewayContext $context;

    protected ClientInterface $client;

    public function __construct(ClientInterface $client, GatewayContext $gatewayContext)
    {
        $this->client = $client;
        $this->context = $gatewayContext;
    }

    /**
     * @throws InvalidApiRequestException
     * @throws InvalidApiResultException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function createBasket(Basket $basket): ?array
    {
        $response = $this->request(CreateBasketRequest::fromBasket($basket, $this->context));

        if ($response->getStatusCode() >= 500) {
            throw new InvalidApiResultException($response->getStatusCode());
        }

        $response = CreateBasketResponse::fromHttpResponse($response);

        if ($response->status >= 400) {
            throw new InvalidApiRequestException($response->status, $response->message);
        }

        if (!$response->success) {
            return null;
        }

        return $response->basket;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function request(RequestInterface $request): ResponseInterface
    {
        $request = $request->withAddedHeader('Authorization', 'Bearer ' . $this->context->getToken());
        return $this->client->sendRequest($request);
    }
}